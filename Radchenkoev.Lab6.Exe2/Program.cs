﻿using System;

namespace Radchenkoev.Lab6.Exe2
{
    class Program
    {
            static void Main(string[] args)
            {
                try
                {
                    Console.WriteLine("Введите размерность матрицы");
                    int n = int.Parse(Console.ReadLine());
                    Console.WriteLine("Введите пределы определения чисел в матрице");
                    int a = int.Parse(Console.ReadLine());
                    int b = int.Parse(Console.ReadLine());
                    Random random = new Random();
                    double[,] mass = new double[n, n];
                    int c = mass.GetLength(0);
                    Console.WriteLine($"Исходная матрица:");
                    for (int i = 0; i < c; i++)
                    {
                        for (int j = 0; j < c; j++)
                        {
                            mass[i, j] = random.Next(a, b);
                            Console.Write($"{mass[i, j]}\t");
                        }
                        Console.WriteLine();
                    }
                    int minValue = 0;
                    int minValue1 = 0;
                    Console.WriteLine($"Полученные значения из главной и побочной диагоналей:");
                    for (int i = 0; i < c; i++)
                    {
                        Console.Write(mass[i, i] + "  ");
                        Console.Write(mass[i, c - i - 1] + "  ");
                    }
                    for (int i = 0; i < c; i++)
                    {
                        if (mass[i, i] > mass[minValue, minValue1])
                        {
                            minValue = i;
                            minValue1 = i;
                        }
                        if (mass[i, c - i - 1] > mass[minValue, minValue1])
                        {
                            minValue = i;
                            minValue1 = c - i - 1;
                        }
                    }
                    Console.WriteLine();
                    double s = mass[minValue, minValue1];
                    mass[minValue, minValue1] = mass[n / 2, n / 2];
                    mass[n / 2, n / 2] = s;
                    Console.WriteLine($"Полученная матрица:");
                    for (int i = 0; i < c; i++)
                    {
                        for (int j = 0; j < c; j++)
                        {
                            Console.Write($"{mass[i, j]}\t");
                        }
                        Console.WriteLine();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine($"ошибка ввода, введите другое значение");
                }
            }
        }
    }


