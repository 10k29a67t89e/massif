﻿using System;

namespace radchenkoev.lab6.exe1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine($"Введите количество элементов в массиве n.");
                int n = Convert.ToInt32(Console.ReadLine());
                int[] array = new int[n];
                int[] Array = new int[n];
                Console.WriteLine($"Введите границы массива а и b.");
                int a = Convert.ToInt32(Console.ReadLine());
                int b = Convert.ToInt32(Console.ReadLine());
                Random r = new Random();
                Console.Write($"Исходный масиив:");
                Console.WriteLine();
                for (var i = 0; i < array.Length; i++)
                {
                    array[i] = r.Next(a, b);
                    Console.Write(array[i] + "  ");
                }
                Console.WriteLine();
                Console.Write($"Полученный массив:");
                Console.WriteLine();
                for (var i = 0; i < array.Length; i++)
                {
                    if (array[i] > 0)
                    {
                        Array[i] = array[i] * array[i];
                    }
                    else
                    {
                        Array[i] = 2 * array[i];
                    }
                    Console.Write(Array[i] + "  ");
                }
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine($"ошибка ввода, введите другое значение");
            }


        }
    }
}
